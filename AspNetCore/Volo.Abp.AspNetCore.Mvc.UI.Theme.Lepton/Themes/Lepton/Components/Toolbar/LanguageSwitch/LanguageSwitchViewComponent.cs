﻿using Microsoft.AspNetCore.Mvc;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Volo.Abp.Localization;

namespace Volo.Abp.AspNetCore.Mvc.UI.Theme.Lepton.Themes.Lepton.Components.Toolbar.LanguageSwitch
{
    public class LanguageSwitchViewComponent : LeptonViewComponentBase
	{
		private readonly ILanguageProvider _languageProvider;

		public LanguageSwitchViewComponent(ILanguageProvider languageProvider)
		{
			this._languageProvider = languageProvider;
		}

		public async Task<IViewComponentResult> InvokeAsync()
		{
			var readOnlyList = await this._languageProvider.GetLanguagesAsync();
			var currentLanguage = readOnlyList.FindByCulture<LanguageInfo>(CultureInfo.CurrentCulture.Name, CultureInfo.CurrentUICulture.Name);
			LanguageSwitchViewComponentModel model = new LanguageSwitchViewComponentModel
			{
				CurrentLanguage = currentLanguage,
				OtherLanguages = (from l in readOnlyList
								  where l != currentLanguage
								  select l).ToList<LanguageInfo>()
			};
			return base.View<LanguageSwitchViewComponentModel>("~/Themes/Lepton/Components/Toolbar/LanguageSwitch/Default.cshtml", model);
		}
	}
}
