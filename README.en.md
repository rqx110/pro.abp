# pro.abp

#### Description
缘由：由于abp vnext商业源码版价格太贵，需要5k刀，还不卖给我，让人火大。可很多资源都可以直接从官网获取，比如商业版的dll可以从https://abp.io/packages获取，模块说明可以从https://docs.abp.io/api-docs/commercial/3.0/api/index.html获取，那我就直接拉下来，反编译后学习他的思路，撸出自己的源码出来，放到开源社区，供广大.net爱好者学习，也欢迎大家参与修复存在的bug，新增新功能模块。

声明：本仓库的开源代码，只为了供大家研究、学习，不允许商业使用，如果由于用于商业项目带来了法律风险，本仓库概不负责，由相应的法律主体承担所有责任。

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
