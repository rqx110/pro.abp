﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Volo.Abp.AutoMapper;
using Volo.Abp.Caching;
using Volo.Abp.Domain;
using Volo.Abp.Domain.Entities.Events.Distributed;
using Volo.Abp.LanguageManagement.Localization;
using Volo.Abp.Localization;
using Volo.Abp.Localization.ExceptionHandling;
using Volo.Abp.Modularity;
using Volo.Abp.ObjectExtending;
using Volo.Abp.ObjectExtending.Modularity;

namespace Volo.Abp.LanguageManagement
{
    [DependsOn(
		typeof(LanguageManagementDomainSharedModule),
		typeof(AbpAutoMapperModule),
		typeof(AbpDddDomainModule),
		typeof(AbpCachingModule)
	)]
	public class LanguageManagementDomainModule : AbpModule
	{
		public override void PreConfigureServices(ServiceConfigurationContext context)
		{
			ModuleExtensionConfigurationHelper.ApplyEntityConfigurationToEntity(LanguageManagementModuleExtensionConsts.ModuleName, LanguageManagementModuleExtensionConsts.EntityNames.Language, typeof(Language));
		}

		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			base.Configure<AbpLocalizationOptions>(options => options.GlobalContributors.Add<DynamicLocalizationResourceContributor>());
			base.Configure<AbpExceptionLocalizationOptions>(options => options.MapCodeNamespace("Volo.Abp.LanguageManagement", typeof(LanguageManagementResource)));
			context.Services.AddAutoMapperObjectMapper<LanguageManagementDomainModule>();
			base.Configure<AbpAutoMapperOptions>(options => options.AddProfile<LanguageManagementDomainAutoMapperProfile>(true));
			base.Configure<AbpDistributedEntityEventOptions>(options =>
			{
				options.EtoMappings.Add<Language, LanguageEto>(typeof(LanguageManagementDomainModule));
				options.EtoMappings.Add<LanguageText, LanguageTextEto>(typeof(LanguageManagementDomainModule));
			});
		}
	}
}
