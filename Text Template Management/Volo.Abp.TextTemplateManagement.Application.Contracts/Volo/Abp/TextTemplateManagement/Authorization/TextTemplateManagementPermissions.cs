﻿using Volo.Abp.Reflection;

namespace Volo.Abp.TextTemplateManagement.Authorization
{
    public class TextTemplateManagementPermissions
	{
		public static string[] GetAll()
		{
			return ReflectionHelper.GetPublicConstantsRecursively(typeof(TextTemplateManagementPermissions));
		}

		public const string GroupName = "TextTemplateManagement";

		public static class TextTemplates
		{
			public const string Default = "TextTemplateManagement.TextTemplates";

			public const string EditContents = "TextTemplateManagement.TextTemplates.EditContents";
		}
	}
}
