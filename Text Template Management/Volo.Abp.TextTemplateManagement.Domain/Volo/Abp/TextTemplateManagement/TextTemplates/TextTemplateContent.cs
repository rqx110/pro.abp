﻿using System;
using Volo.Abp.Domain.Entities.Auditing;
using Volo.Abp.MultiTenancy;

namespace Volo.Abp.TextTemplateManagement.TextTemplates
{
    public class TextTemplateContent : FullAuditedAggregateRoot<Guid>, IMultiTenant
	{
		public virtual Guid? TenantId { get; set; }

		public virtual string Name { get; private set; }

		public virtual string CultureName { get; private set; }

		public virtual string Content { get; private set; }

		public TextTemplateContent(Guid id, string name, string content, string cultureName = null, Guid? tenantId = null)
		{
			this.SetName(name);
			this.SetCultureName(cultureName);
			this.SetContent(content);
			this.TenantId = tenantId;
		}

		public virtual void SetName(string name)
		{
			this.Name = Check.NotNullOrWhiteSpace(name, nameof(name), TextTemplateConsts.MaxNameLength, 0);
		}

		public virtual void SetCultureName(string cultureName)
		{
			this.CultureName = ((cultureName == null) ? null : Check.Length(cultureName, nameof(cultureName), TextTemplateConsts.MaxCultureNameLength, TextTemplateConsts.MinCultureNameLength));
		}

		public virtual void SetContent(string content)
		{
			this.Content = Check.NotNullOrWhiteSpace(content, nameof(content), TextTemplateConsts.MaxContentLength, 0);
		}
	}
}
