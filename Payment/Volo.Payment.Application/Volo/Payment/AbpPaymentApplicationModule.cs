﻿using Microsoft.Extensions.DependencyInjection;
using Volo.Abp.AutoMapper;
using Volo.Abp.Modularity;

namespace Volo.Payment
{
    [DependsOn(
		typeof(AbpPaymentDomainModule),
		typeof(AbpPaymentApplicationContractsModule),
		typeof(AbpAutoMapperModule)
	)]
	public class AbpPaymentApplicationModule : AbpModule
	{
		public override void ConfigureServices(ServiceConfigurationContext context)
		{
			context.Services.AddAutoMapperObjectMapper<AbpPaymentApplicationModule>();
			base.Configure<AbpAutoMapperOptions>(options => options.AddProfile<PaymentApplicationAutoMapperProfile>(true));
		}
	}
}
