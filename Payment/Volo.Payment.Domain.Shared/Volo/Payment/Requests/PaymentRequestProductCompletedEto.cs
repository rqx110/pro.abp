﻿using System;
using Volo.Abp.Domain.Entities.Events.Distributed;

namespace Volo.Payment.Requests
{
    [Serializable]
	public class PaymentRequestProductCompletedEto : EtoBase
	{
		public string Code { get; set; }

		public string Name { get; set; }

		public int Count { get; set; }
	}
}
